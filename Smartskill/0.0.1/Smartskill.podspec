Pod::Spec.new do |spec|

  spec.name         = "Smartskill"
  spec.version      = "0.0.1"
  spec.summary      = "The iOS library specification for SmartSkill Framework."
  spec.description  = <<-DESC
  This is the iOS library for SmartSkill by Enparadigm
                   DESC
  spec.homepage     = "https://www.enparadigm.com"
  spec.license      = "ENPARADIGM"
  spec.author             = { "Anshul Gupta" => "anshul.gupta@enparadigm.com" }
  spec.platform     = :ios
  spec.ios.deployment_target = "9.0"
  spec.source       = { :git => "https://gitlab.com/anshul.gupta3/test-xcframework.git", :tag => "#{spec.version}" }
  #spec.source_files = "Smartskill.xcframework/*/Smartskill.xcframework/Headers/*.h"
  #spec.source_files  = "SmartSkill.framework/Headers/*.h"
  spec.vendored_frameworks = "Smartskill.xcframework"
  spec.swift_version = '5.0'
  spec.pod_target_xcconfig = { 'SWIFT_VERSION' => '5' }
 # spec.public_header_files = "SmartSkill.framework/Headers/*.h"
  spec.library   = "sqlite3"
  spec.requires_arc = true
  #spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  #spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.preserve_paths = 'Smartskill.xcframework'
  #spec.module_map = 'SmartSkill.xcframework/Modules/module.modulemap'

  spec.dependency "SDWebImage"
  spec.dependency "Alamofire"
  spec.dependency "Kingfisher","~> 4.6.4"
  spec.dependency "TTFortuneWheel"
  spec.dependency "DropDown"
  spec.dependency "Koloda"
  spec.dependency "Cartography"
  spec.dependency "MBRadioButton"
  spec.dependency "lottie-ios"
  spec.dependency "SwiftSVG"
  spec.dependency "JJFloatingActionButton"
  spec.dependency "YoutubePlayer-in-WKWebView"
  spec.dependency "KeychainSwift"

end

